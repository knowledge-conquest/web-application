const FightLogHelper = {
  props: {
    entities: { type: Array, default: () => [] },
  },
  methods: {
    entity(key) {
      for (let i = 0; i < this.entities.length; i += 1) {
        if (this.entities[i].key === key) {
          return this.entities[i];
        }
      }
      return null;
    },
    entitySkill(entityKey, skillKey) {
      const { skills } = this.entity(entityKey);
      for (let i = 0; i < skills.length; i += 1) {
        if (skills[i].key === skillKey) {
          return skills[i];
        }
      }
      return null;
    },
    entityName(key) {
      const entity = this.entity(key);
      return entity ? entity.name : 'Entity';
    },
  },
};

export default FightLogHelper;
