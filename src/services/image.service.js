const ImageService = {
  getItemIcon({ type, itemData }) {
    switch (type) {
      case 'RESOURCE':
        return this.getResourceIcon(itemData);
      case 'WEAPON':
        return this.getWeaponIcon(itemData);
      case 'ARMOUR':
        return this.getArmorIcon(itemData);
      default:
        return this.getNotFoundIcon();
    }
  },

  getItemDataIcon(itemData) {
    switch (itemData.type) {
      case 'RESOURCE_DATA':
        return this.getResourceIcon(itemData);
      case 'WEAPON_DATA':
        return this.getWeaponIcon(itemData);
      case 'ARMOUR_DATA':
        return this.getArmorIcon(itemData);
      default:
        return this.getNotFoundIcon();
    }
  },

  getResourceIcon({ resourceType }) {
    switch (resourceType) {
      case 'WOOD':
        return '/images/resources/wood-icon.svg';
      case 'ROCK':
        return '/images/resources/rock-icon.svg';
      case 'METAL':
        return '/images/resources/metal-icon.svg';
      case 'PLANT':
        return '/images/resources/plant-icon.svg';
      default:
        return this.getNotFoundIcon();
    }
  },

  getArmorIcon({ equipmentSlot }) {
    switch (equipmentSlot) {
      case 'HELMET':
        return '/images/armours/helmet-icon.svg';
      case 'CHEST_PLATE':
        return '/images/armours/chestplate-icon.svg';
      case 'GLOVES':
        return '/images/armours/gloves-icon.svg';
      case 'PANTS':
        return '/images/armours/pants-icon.svg';
      case 'BOOTS':
        return '/images/armours/boots-icon.svg';
      default:
        return this.getNotFoundIcon();
    }
  },

  getWeaponIcon({ weaponType }) {
    switch (weaponType) {
      case 'SWORD':
        return '/images/weapons/sword-icon.svg';
      case 'DAGGER':
        return '/images/weapons/dagger-icon.svg';
      case 'AXE':
        return '/images/weapons/axe-icon.svg';
      case 'HAMMER':
        return '/images/weapons/hammer-icon.svg';
      case 'SPEAR':
        return '/images/weapons/spear-icon.svg';
      case 'STAFF':
        return '/images/weapons/staff-icon.svg';
      case 'SCYTHE':
        return '/images/weapons/scythe-icon.svg';
      case 'BOW':
        return '/images/weapons/bow-icon.svg';
      default:
        return this.getNotFoundIcon();
    }
  },

  getAreaIcon(areaType) {
    switch (areaType) {
      case 'SPAWN_AREA':
        return '/images/areas/spawn-icon.svg';
      case 'CRAFTING_AREA':
        return '/images/areas/crafting-icon.svg';
      case 'GATHERING_AREA':
        return '/images/areas/gathering-icon.svg';
      case 'TRAINING_AREA':
        return '/images/areas/training-icon.svg';
      case 'SKILL_PRACTICE_AREA':
        return '/images/areas/skill-practice-icon.svg';
      case 'CULTIVATION_AREA':
        return '/images/areas/cultivation-icon.svg';
      case 'LOOT_TREASURE_AREA':
        return '/images/areas/loot-treasure-icon.svg';
      case 'SKILL_TREASURE_AREA':
        return '/images/areas/skill-treasure-icon.svg';
      case 'EMPTY_AREA':
        return '/images/areas/empty-icon.svg';
      case 'TELEPORTATION_AREA':
        return '/images/areas/teleportation-icon.svg';
      case 'MONSTER_SPAWNER_AREA':
        return '/images/areas/spawner-icon.svg';
      case 'MONSTER_BOSS_AREA':
        return '/images/areas/boss-icon.svg';
      default:
        return this.getNotFoundIcon();
    }
  },

  getSkillIcon() {
    return '/images/skills/skill-icon.svg';
  },

  getActionIcon(actionState) {
    return `/images/actions/${actionState.toLowerCase().replace('_', '-')}-icon.svg`;
  },

  getMenuIcon(icon) {
    return `/images/menu/${icon}-icon.svg`;
  },

  getDeadIcon() {
    return '/images/dead-icon.svg';
  },

  getNotFoundIcon() {
    return '/images/not-found-icon.svg';
  },
};

export default ImageService;
