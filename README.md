# Knowledge conquest Web Application

## Environment variables

This project currently uses two environment variables:
- VUE\_APP\_AUTHENTICATION\_SERVICE: contains an http url pointing to the authentication service.
- VUE\_APP\_ADVENTURER\_SERVICE: contains an http url pointing to the adventurer service.

## Usage

First install the dependencies by running the command below inside the project folder (same as this readme).
```
npm install
```

Then the application can be run localy in development mode like this:
```
npm run serve
```

To run the tests use this command:
```
npm run test:unit
```

This project uses ESLint. To check the project lint and fix auto-fixable errors use:
```
npm run lint
```

For deployment, the application must be built with this command:
```
npm run build
```

This creates a `dist` folder. This folder can be placed inside a server that deploys static files. The `dockerfile` creates a Nginx image serving the `dist` folder.

On every commit to the branch `master` the application is built and a docker image with the tag `dev` is update on the repository's docker registery.
