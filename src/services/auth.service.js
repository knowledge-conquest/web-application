import ApiService from './api.service';
import JWTService from './jwt.service';

const AUTH_URL = process.env.VUE_APP_AUTHENTICATION_SERVICE;
const USER_ID_KEY = 'userId';

const AuthService = {
  getPath(path) {
    return `${AUTH_URL}${path}`;
  },

  /**
   * Send a login request via API service.
   * @param {String} username User's username.
   * @param {String} password User's password.
   */
  login(username, password) {
    return ApiService.post(
      this.getPath('/authentication'),
      { username, password },
    ).then(({ status, data }) => {
      if (status === 200) {
        JWTService.storeToken(data.token);
        this.storeUserId(data.userId);
        return true;
      }

      this.removeUserId();
      return false;
    });
  },

  /**
   * Send a register request via API service.
   * @param {String} email User's email.
   * @param {String} password User's password.
   */
  register(email, username, password) {
    return ApiService.post(
      this.getPath('/users'),
      { email, username, password },
    ).then((response) => {
      if (response.status === 200) {
        return this.login(username, password);
      }
      return false;
    });
  },

  /**
   * Fetches the informations of a user.
   * Require its the user to be logged in before.
   */
  async getUser() {
    const id = this.getUserId();
    if (id) {
      return ApiService.get(this.getPath(`/users/${id}`), true, { 'Content-Type': 'application/json' })
        .then((response) => {
          if (response.status === 200) {
            return response.data;
          }
          return null;
        });
    }

    return null;
  },

  /**
   * Saves the user id in the local storage.
   * @param id User ID.
   */
  storeUserId(id) {
    window.localStorage.setItem(USER_ID_KEY, id);
  },
  /**
   * Removes the user id from the local storage
   */
  removeUserId() {
    window.localStorage.removeItem(USER_ID_KEY);
  },
  /**
   * Retrieves the user id.
   * @return The user id or null it does not exist.
   */
  getUserId() {
    return window.localStorage.getItem(USER_ID_KEY) || null;
  },
};

export default AuthService;
