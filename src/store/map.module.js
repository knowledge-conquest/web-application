import { make } from 'vuex-pathify';
import AdventurerService from '../services/adventurer.service';

// map Module

export const state = {
  region: null,
  area: null,
  regions: null,
  areas: [],
  edges: [],
  fetched: false,
};

export const getters = {
  ...make.getters(state),
};

export const mutations = {
  ...make.mutations(state),
};

export const actions = {
  async fetchRealmMap({ commit, dispatch }, radius) {
    await dispatch('clearRealmMap');
    return AdventurerService.getRealmMap(AdventurerService.getAdventurerId(), radius)
      .then((data) => {
        if (data) {
          commit('SET_REGIONS', data);
        }
      });
  },

  async fetchRegionMap({ commit, dispatch }) {
    await dispatch('clearRegionMap');
    return Promise.all([
      AdventurerService.getRegionMap(AdventurerService.getAdventurerId()),
      AdventurerService.getLocation(AdventurerService.getAdventurerId()),
    ]).then(([data, area]) => {
      if (data && area) {
        commit('SET_REGION', data.region);
        commit('SET_AREA', area);
        commit('SET_AREAS', data.areas);
        commit('SET_EDGES', data.edges);
        commit('SET_FETCHED', true);
      }
    });
  },

  clearRealmMap({ commit }) {
    commit('SET_REGIONS', null);
  },

  clearRegionMap({ commit }) {
    commit('SET_REGION', null);
    commit('SET_AREA', null);
    commit('SET_AREAS', []);
    commit('SET_EDGES', []);
    commit('SET_FETCHED', false);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
