import Vue from 'vue';
import Vuex from 'vuex';
import pathify from 'vuex-pathify';

import user from './user.module';
import player from './player.module';
import map from './map.module';

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [pathify.plugin],
  modules: {
    user,
    player,
    map,
  },
});
