module.exports = {
  theme: {
    extend: {
      colors: {
        tblack: 'rgba(0, 0, 0, 0.8)', // main text blac,
        twhite: 'rgba(255, 255, 255, 0.9)', // main text white
      },
    },
  },
  variants: {},
  plugins: [],
};
