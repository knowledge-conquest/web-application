import Vue from 'vue';
import VueI18n from 'vue-i18n';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import router from './router';
import store from './store';
import App from './App.vue';
import ApiService from './services/api.service';
import '@/assets/css/tailwind.css';

// Translations
import ENGLISH_TRANSLATION from './locale/en';

ApiService.init();

Vue.use(VueI18n);

Vue.component('fa-icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  i18n: new VueI18n({
    locale: 'en',
    messages: {
      en: ENGLISH_TRANSLATION,
    },
  }),
  render: (h) => h(App),
}).$mount('#app');
