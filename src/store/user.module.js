import { make } from 'vuex-pathify';
import JWTService from '../services/jwt.service';
import AuthService from '../services/auth.service';

// user Module
// namespace: user

export const state = {
  user: null,
};

export const getters = {
  ...make.getters(state),

  /**
   * Checks if the user is authenticated.
   */
  isAuthenticated(state) {
    return state.user != null;
  },
};

export const mutations = {
  ...make.mutations(state),
};

export const actions = {
  async fetchUser({ commit }) {
    if (!AuthService.getUserId()) {
      return false;
    }

    return AuthService.getUser()
      .then((user) => {
        commit('SET_USER', user);
        return user !== null;
      });
  },

  async checkAuth({ dispatch }) {
    return JWTService.getToken() ? dispatch('fetchUser') : dispatch('clearAuth');
  },

  /**
   * Clears the authentication data.
   */
  clearAuth({ commit }) {
    JWTService.removeToken();
    AuthService.removeUserId();
    commit('SET_USER', null);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
