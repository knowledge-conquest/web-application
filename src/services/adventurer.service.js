import ApiService from './api.service';

const SERVICE_URL = process.env.VUE_APP_ADVENTURER_SERVICE;
const ADVENTURE_ID_KEY = 'adventurerId';

const AdventurerService = {
  getPath(path) {
    return `${SERVICE_URL}${path}`;
  },

  /* ADVENTURERS */

  async createRealm(id) {
    if (id) {
      return ApiService.post(this.getPath(`/adventurers/${id}/realm`), {}, true)
        .then(({ status, data }) => {
          if (status === 200) {
            return data;
          }
          return null;
        });
    }

    return null;
  },

  async getAdventureractionState(id) {
    if (id) {
      return ApiService.get(this.getPath(`/adventurers/${id}/state`), true)
        .then(({ status, data }) => {
          if (status === 200) {
            return data;
          }
          return null;
        });
    }

    return null;
  },

  async getWeaponProficiencies(id) {
    if (id) {
      return ApiService.get(this.getPath(`/adventurers/${id}/proficiencies`), true)
        .then(({ status, data }) => {
          if (status === 200) {
            return data;
          }
          return null;
        });
    }

    return null;
  },

  async getAdventurerAction(id, actionState) {
    if (!id) return null;

    let request = null;
    switch (actionState) {
      case 'FIGHT':
        request = ApiService.get(this.getPath(`/adventurers/${id}/fight`), true);
        break;
      case 'CULTIVATION':
        request = ApiService.get(this.getPath(`/adventurers/${id}/idle/cultivation`), true);
        break;
      case 'TRAINING':
        request = ApiService.get(this.getPath(`/adventurers/${id}/idle/training`), true);
        break;
      case 'SKILL_PRACTICE':
        request = ApiService.get(this.getPath(`/adventurers/${id}/idle/skill_practice`), true);
        break;
      case 'SPAWN_RECOVERY':
        request = ApiService.get(this.getPath(`/adventurers/${id}/idle/spawn_recovery`), true);
        break;
      default:
        return null;
    }

    return request.then(({ status, data }) => {
      if (status === 200) {
        return data;
      }
      return null;
    });
  },

  async startAdventurerIdleAction(id, actionState, data) {
    let request = null;
    switch (actionState) {
      case 'CULTIVATION':
        request = ApiService.post(this.getPath(`/adventurers/${id}/idle/cultivation`), {}, true);
        break;
      case 'TRAINING':
        request = ApiService.post(this.getPath(`/adventurers/${id}/idle/training/${data.toLowerCase()}`), {}, true);
        break;
      case 'SKILL_PRACTICE':
        request = ApiService.post(this.getPath(`/adventurers/${id}/idle/skill_practice/${data}`), {}, true);
        break;
      case 'SPAWN_RECOVERY':
        request = ApiService.post(this.getPath(`/adventurers/${id}/idle/spawn_recovery`), {}, true);
        break;
      default:
        request = null;
        break;
    }

    if (id && request) {
      return request.then(({ status, data }) => {
        if (status === 200) {
          return data;
        }
        return null;
      });
    }

    return null;
  },

  async stopAdventurerIdleAction(id, actionState) {
    let request = null;
    switch (actionState) {
      case 'CULTIVATION':
        request = ApiService.delete(this.getPath(`/adventurers/${id}/idle/cultivation`), true);
        break;
      case 'TRAINING':
        request = ApiService.delete(this.getPath(`/adventurers/${id}/idle/training`), true);
        break;
      case 'SKILL_PRACTICE':
        request = ApiService.delete(this.getPath(`/adventurers/${id}/idle/skill_practice`), true);
        break;
      case 'SPAWN_RECOVERY':
        request = ApiService.delete(this.getPath(`/adventurers/${id}/idle/spawn_recovery`), true);
        break;
      default:
        request = null;
        break;
    }

    if (id && request) {
      return request.then(({ status, data }) => {
        if (status === 200) {
          return data;
        }
        return null;
      });
    }

    return null;
  },

  async getAdventurer() {
    const id = this.getAdventurerId();
    if (id) {
      return ApiService.get(this.getPath(`/adventurers/${id}`), true)
        .then(({ status, data }) => {
          if (status === 200) {
            return data;
          }
          return null;
        });
    }

    return null;
  },

  fetchAdventurersOf(id) {
    return ApiService.get(this.getPath(`/adventurers/of/${id}`), true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return [];
      });
  },

  create(name) {
    return ApiService.post(this.getPath(`/adventurers?name=${name}`), {}, true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return false;
      });
  },

  /* FIGHT */

  async getFight(adventurerId) {
    return ApiService.get(this.getPath(`/adventurers/${adventurerId}/fight`), true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  async attack(adventurerId, entityTargetId, skillSlotIndex) {
    return ApiService.post(
      this.getPath(`/adventurers/${adventurerId}/fight/attack/${entityTargetId}?skill=${skillSlotIndex}`), {}, true,
    ).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  async startMonsterAreaFight(adventurerId) {
    return ApiService.post(this.getPath(`/adventurers/${adventurerId}/fight`), {}, true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  /* TRAVEL */

  async getRealmMap(id, radius) {
    return ApiService.get(this.getPath(`/adventurers/${id}/realm/map?radius=${radius}`), true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  async getRegionMap(id) {
    return ApiService.get(this.getPath(`/adventurers/${id}/region/map`), true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  async getLocation(id) {
    return ApiService.get(this.getPath(`/adventurers/${id}/location`), true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  async travel(adv, dest) {
    return ApiService.post(this.getPath(`/adventurers/${adv}/travel/${dest}`), {}, true)
      .then(({ status, data }) => {
        if (status === 200) {
          return data;
        }

        return null;
      });
  },

  async teleport(adv) {
    return ApiService.post(this.getPath(`/adventurers/${adv}/teleport`), {}, true)
      .then(({ status }) => status === 200);
  },

  /* CRAFTING */

  async getAreaRecipes(adv, offset, count) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/recipes?offset=${offset}&count=${count}`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async getRecipeIngredients(adv, recipe) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/recipes/${recipe}/ingredients`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async craft(adv, recipe) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/craft/${recipe}?count=1`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  /* GATHERING */

  async gatheringArea(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/location/gathering`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  async gather(adv) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/gather`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  /* Treasure */

  async lootTreasureArea(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/treasure/loot`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  async openLootTreasure(adv) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/treasure/loot/open`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async skillTreasureArea(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/treasure/skill`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  async openSkillTreasure(adv) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/treasure/skill/open`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  /* INVENTORIES */

  async getResources(adv, offset, count, sort) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/resources?offset=${offset}&count=${count}&sort=${sort}`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async getWeapons(adv, offset, count, sort) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/weapons?offset=${offset}&count=${count}&sort=${sort}`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async getWornWeapon(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/wornweapon`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return null;
    });
  },

  async getArmours(adv, offset, count, sort) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/armours?offset=${offset}&count=${count}&sort=${sort}`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async getWornArmours(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/wornarmours`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async equip(adv, equipment, slot) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/equip/${equipment}/${slot}`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return false;
    });
  },

  async unEquip(adv, slot) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/unequip/${slot}`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return false;
    });
  },

  /* SKILLS INVENTORY */

  async getSkills(adv, offset, count, sort) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/skills?offset=${offset}&count=${count}&sort=${sort}`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async getEquippedSkills(adv) {
    return ApiService.get(this.getPath(
      `/adventurers/${adv}/skills/equipped`,
    ), true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return [];
    });
  },

  async equipSkill(adv, skill, slot) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/skills/${skill}/equip/${slot}`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return false;
    });
  },

  async unEquipSkill(adv, slot) {
    return ApiService.post(this.getPath(
      `/adventurers/${adv}/skills/unequip/${slot}`,
    ), {}, true).then(({ status, data }) => {
      if (status === 200) {
        return data;
      }

      return false;
    });
  },

  /* local storage */

  storeAdventurerId(id) {
    window.localStorage.setItem(ADVENTURE_ID_KEY, id);
  },

  removeAdventurerId() {
    window.localStorage.removeItem(ADVENTURE_ID_KEY);
  },

  getAdventurerId() {
    return window.localStorage.getItem(ADVENTURE_ID_KEY) || null;
  },
};

export default AdventurerService;
