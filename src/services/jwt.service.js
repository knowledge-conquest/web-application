const TOKEN_KEY = 'jwtToken';

const JWTService = {
  /**
   * Retrieves the authorization token.
   * @return The token or null if the token does not exist.
   */
  getToken() {
    return window.localStorage.getItem(TOKEN_KEY) || null;
  },
  /**
   * Saves the authorizationtoken in the local storage.
   * @param token Authorization token.
   */
  storeToken(token) {
    window.localStorage.setItem(TOKEN_KEY, token);
  },
  /**
   * Removes the authorization token from the local storage
   */
  removeToken() {
    window.localStorage.removeItem(TOKEN_KEY);
  },
  /**
   * Creates an authorization header.
   * @return The authorization header or an empty header if there is no token.
   */
  getAuthHeader() {
    const token = JWTService.getToken();
    return token ? { Authorization: `Bearer ${token}` } : {};
  },
};

export default JWTService;
