import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import JWTService from './services/jwt.service';

Vue.use(Router);

// Middlwares

function createMiddleware(redirect, check) {
  return { redirect, check };
}

function checkMidlewares(middlewares) {
  return (to, from, next) => {
    if (middlewares) {
      for (let i = 0; i < middlewares.length; i += 1) {
        if (!middlewares[i].check(to, from, next)) {
          next(middlewares[i].redirect);
          return false;
        }
      }
    }

    next();
    return true;
  };
}

// Checks if the user is connected or redirects to login.
const isConnected = createMiddleware('/login', () => !!JWTService.getToken());

// Checks if the user is disconnected or redirects to profile.
const idDisconnected = createMiddleware('/', () => !JWTService.getToken());

// Routes

const router = new Router({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '/',
      redirect: '/game',
    },
    {
      name: 'game',
      path: '/game',
      component: () => import('./views/Game.vue'),
      beforeEnter: checkMidlewares([isConnected]),
    },
    {
      path: '/game*',
      redirect: '/game',
    },
    {
      name: 'login',
      path: '/login',
      component: () => import('./views/Login.vue'),
      beforeEnter: checkMidlewares([idDisconnected]),
    },
    {
      name: 'register',
      path: '/register',
      component: () => import('./views/Register.vue'),
      beforeEnter: checkMidlewares([idDisconnected]),
    },
    {
      name: 'logout',
      path: '/logout',
      beforeEnter: (to, from, next) => {
        if (checkMidlewares([isConnected])) {
          Promise.all([
            store.dispatch('user/clearAuth'),
            store.dispatch('player/clearSelectedAdventurer'),
          ]).then(() => {
            next('/login');
          });
        }
      },
    },
  ],
});

// Check auth before entering a route
router.beforeEach((to, from, next) => {
  store.dispatch('user/checkAuth').then(() => {
    next();
  });
});

export default router;
