const Pagination = {
  data() {
    return {
      pagination: {
        offset: 0,
        sort: 0,
        count: 20,
        callback: () => {},
      },
    };
  },
  methods: {
    paginationSortBy(index) {
      this.pagination.offset = 0;
      this.pagination.sort = index;
      this.pagination.callback();
    },

    paginationPreviousPage() {
      this.pagination.offset = Math.max(0, this.pagination.offset - this.pagination.count);
      this.pagination.callback();
    },

    paginationNextPage() {
      this.pagination.offset += this.pagination.count;
      this.pagination.callback();
    },
  },
};

export default Pagination;
