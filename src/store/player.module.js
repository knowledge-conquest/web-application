import { make, dispatch } from 'vuex-pathify';
import AdventurerService from '../services/adventurer.service';

// user Module
// namespace: player

export const state = {
  adventurer: null,
  fighting: false,
  action: null,
  actionState: null,
};

export const getters = {
  ...make.getters(state),
};

export const mutations = {
  ...make.mutations(state),
};

export const actions = {

  async fetchActionState({ commit, dispatch }) {
    if (AdventurerService.getAdventurerId()) {
      return AdventurerService.getAdventureractionState(AdventurerService.getAdventurerId())
        .then((actionState) => {
          if (actionState) {
            commit('SET_ACTION_STATE', actionState);
          } else {
            commit('SET_ACTION_STATE', null);
          }
        });
    }

    return false;
  },

  async fetchAction({ commit, getters, dispatch }) {
    if (AdventurerService.getAdventurerId() && getters.actionState) {
      return AdventurerService.getAdventurerAction(AdventurerService.getAdventurerId(), getters.actionState)
        .then((action) => {
          if (action) {
            commit('SET_ACTION', action);
          } else {
            commit('SET_ACTION_TYPE', null);
            commit('SET_ACTION', null);
          }
        });
    }

    return false;
  },

  async startIdleAction({ commit }, { actionState, data }) {
    if (AdventurerService.getAdventurerId() && actionState) {
      return AdventurerService.startAdventurerIdleAction(AdventurerService.getAdventurerId(), actionState, data)
        .then((action) => {
          if (action) {
            commit('SET_ACTION_STATE', actionState);
            commit('SET_ACTION', action);
          }
        });
    }

    return false;
  },

  async stopIdleAction({ commit, getters }) {
    if (AdventurerService.getAdventurerId() && getters.actionState) {
      return AdventurerService.stopAdventurerIdleAction(AdventurerService.getAdventurerId(), getters.actionState)
        .then((action) => {
          if (action) {
            commit('SET_ACTION', action);
          }
        });
    }

    return false;
  },

  async fetchAdventurer({ commit, dispatch }) {
    if (AdventurerService.getAdventurerId()) {
      return AdventurerService.getAdventurer()
        .then((adventurer) => dispatch('fetchActionState')
          .then(() => {
            commit('SET_ADVENTURER', adventurer);
            return adventurer == null;
          }));
    }

    return false;
  },

  async refreshAction({ commit, dispatch }) {
    commit('SET_ACTION_STATE', null);
    commit('SET_ACTION', null);
    return dispatch('fetchActionState');
  },

  async checkSelectedAdventurer({ dispatch }) {
    return AdventurerService.getAdventurerId()
      ? dispatch('fetchAdventurer')
      : dispatch('clearSelectedAdventurer');
  },

  clearSelectedAdventurer({ commit }) {
    AdventurerService.removeAdventurerId();
    commit('SET_ADVENTURER', null);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
