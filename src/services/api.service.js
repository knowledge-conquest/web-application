import axios from 'axios';
import JWTService from './jwt.service';

const API_ERROR = {
  status: 400,
  data: null,
};

/**
 * Helper class for backend API requests.
 */
const ApiService = {
  init() {
    axios.defaults.headers.common.Accept = 'application/json';
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    // 'Access-Control-Allow-Origin': '*',
  },

  /**
   * Sends a get request to the backend api.
   * @param {string} path Path to the resource.
   */
  get(path, withAuth = false) {
    return axios.get(path, {
      headers: withAuth ? this.authorizationHeader() : {},
      data: {},
    }).catch(this.handleError);
  },

  /**
   * Sends a get request to the backend api.
   * @param {string} Path to the resource.
   * @param {Object} data Data to post.
   * @param {bool} withAuth if being authenticated is required
   * @param {key1: value1, ...} optional additional headers
   */
  post(path, data, withAuth = false, additionalHeaders = {}) {
    return axios.post(path, data, {
      headers: {
        ...(withAuth ? this.authorizationHeader() : {}),
        ...additionalHeaders,
      },
    }).catch(this.handleError);
  },

  /**
   * Sends a put request to the backend api.
   * @param {string} Path to the resource.
   * @param {Object} data Data to put.
   */
  put(path, data, withAuth = false) {
    return axios.put(path, data, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  /**
   * Sends a delete request to the backend api.
   * @param {string} Path to the resource.
   */
  delete(path, withAuth = false) {
    return axios.delete(path, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  handleError({ response }) {
    if (response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return response;
    }

    return API_ERROR;
  },

  /**
   * Creates an authorization header if there is a token.
   * Returns an empty object otherwise.
   */
  authorizationHeader() {
    const token = JWTService.getToken();
    return token ? { Authorization: `Bearer ${token}` } : {};
  },
};

export default ApiService;
